import React, { Component } from 'react';

class List extends Component { 


    sendUpdation() { 
        this.props.changeStatus(this.props.index); 
    } 
    
    

    render() { 

        let output = null;

        if( !this.props.obj.status ) { 
            // status - false 
            output = ( 
                <div className="d-flex py-2"> 
                    <h3> 
                        <li> 
                            {this.props.obj.task} 
                        </li> 
                    </h3> 
                   <button className="ml-3 btn-sm btn-dark"  onClick={ () => this.sendUpdation() }> 
                       Finish
                   </button> 
                </div> 
            ) 
        } 
        else if(this.props.obj.status) 
        {
            // status - true 
            output = (
                <div className="d-flex py-2"> 
                    <h3>
                        <li> 
                            { this.props.obj.task } | Finished
                        </li> 
                    </h3> 
                    <button className="ml-3 btn-sm btn-dark"  
                            onClick={ () => this.sendUpdation() }> 
                        Undone 
                    </button> 
                </div> 
            )
        } 

        return ( 
            <div>
                { output } 
            </div>
        ) 
    } 
} 

export default List;



