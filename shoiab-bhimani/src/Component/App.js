import React, { Component } from 'react';
import List from './List/list';
import './App.css';

class App extends Component { 
  

  constructor(props) {
    super(props);

    this.state = { 
      todos : [ 
        { 
          task: 'Solve this',
          status: false
        }, 
        { 
          task: 'Go to university',
          status: false
        }, 
        { 
          task: 'Take Breakfast',
          status: false
        }, 
      ] 
    } 
  } 


  updateStatus(index) { 
    let list = this.state.todos[index];
    list.status = !list.status;

    this.setState({ 
      ...this.state 
    }); 
  } 

  
  render() { 
    return ( 
      <div className="App container pt-5 mt-5"> 
        <h1 className="display-4 pb-3">My todo list...</h1> 
        
        <ul> 
          { 
              this.state.todos.map((todo, i) => { 
              return ( <List obj={ todo } key={ i } index={ i }
                              changeStatus={() => this.updateStatus(i)}/> ) 
            }) 
          } 
        </ul> 
      </div> 
    ); 
  } 
} 

export default App;










